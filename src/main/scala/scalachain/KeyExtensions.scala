package scalachain

import java.security.PublicKey

object KeyExtensions {

  implicit class ExtendedPublicKey(publicKey: PublicKey) {

    // TODO: use base58
    lazy val keyString =
      publicKey.toString.replace("Sun RSA public key, 512 bits\n  modulus: ", "")
       .replace("public exponent: 65537", "").trim

  }

}
