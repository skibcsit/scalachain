package scalachain

import java.security._
import java.util.Random

import Node.{CreateGenesisBlock, MakeTransaction}
import akka.actor.{ActorRef, ActorSystem}
import akka.routing.BroadcastGroup

import scala.collection.mutable.ListBuffer
import scala.io.StdIn

import KeyExtensions.ExtendedPublicKey

object Main  {

  def keyPairToTuple(p: KeyPair): Tuple2[PublicKey, PrivateKey] =
    (p.getPublic, p.getPrivate)

  def main(args: Array[String]): Unit = {
    val blockchainSystem = ActorSystem("blockchainSystem")

    //allows generating random (unique) key pairs each time
//    val random = SecureRandom.getInstance("NativePRNG", "SUN")
    val generator = KeyPairGenerator.getInstance("RSA")
//    generator.initialize(512, random)
    generator.initialize(512)

    val (keyPairs, paths) = (
      for {
        _ <- 1 to 4
        keyPair = generator.genKeyPair()
        path = "/user/" + keyPair.getPublic.keyString
      } yield keyPair -> path
    ).unzip

    val pathsImmutable = paths.toList
    lazy val router = blockchainSystem.actorOf(BroadcastGroup(pathsImmutable).props(), "router")
    println(router.path) //DO NOT TOUCH! (somehow, omitting this line causes errors)

    val headNode = createNode(blockchainSystem, keyPairs.head)
    headNode ! CreateGenesisBlock

    val tailNodes =
      for {
        keyPair <- keyPairs.tail
        node = createNode(blockchainSystem, keyPair)
      } yield node

    val nodes = headNode :: tailNodes.toList

    lazy val rand = new Random(System.currentTimeMillis())
    for (_ <- 1 to 10) {
      val node = nodes(rand.nextInt(nodes.length))
      node ! MakeTransaction(Map(keyPairs(rand.nextInt(keyPairs.size)).getPublic -> 10))
    }

    try {
      StdIn.readLine()
    } finally {
      blockchainSystem.terminate()
    }
  }

  private def createNode(blockchainSystem: ActorSystem, keyPair: KeyPair) =
    blockchainSystem
      .actorOf(Node.props(keyPairToTuple(keyPair)), keyPair.getPublic.keyString)

}
